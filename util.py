from enum import Enum

CURRENCY_NAME   = "Dollar"
CURRENCY_SHORT  = "$"
WEIGHT_LONG     = "Kilogramm"
WEIGHT_SHORT    = "kg"

NODE_NAME_PATH      = "ressources/node_names.yaml"
TRADER_NAME_PATH    = "ressources/trader_names.yaml"
ITEM_INFOS_PATH     = "ressources/item_infos.yaml"

FLOAT_PAD_EXTRA = 2

#https://misc.flogisoft.com/bash/tip_colors_and_formatting

class bcolors:
    RED = 31
    GREEN = 32
    YELLOW = 33
    BLUE = 34
    MAGENTA = 35
    CYAN = 36
    LIGHTGRAY = 37
    DARKGRAY = 90
    LIGHTRED = 91
    LIGHTGREEN = 92
    LIGHTYELLOW = 93
    LIGHTBLUE = 94
    LIGHTMAGENTA = 95
    LIGHTCYAN = 96
    ENDC = 0
    BACK_RED = 41
    BACK_GREEN = 42
    BACK_YELLOW = 43
    BACK_BLUE = 44
    BACK_MAGENTA = 45
    BACK_CYAN = 46
    BACK_LIGHTGRAY = 47
    BACK_DARKGRAY = 100
    BACK_LIGHTRED = 101
    BACK_LIGHTGREEN = 102
    BACK_LIGHTYELLOW = 103
    BACK_LIGHTBLUE = 104
    BACK_LIGHTMAGENTA = 105
    BACK_LIGHTCYAN = 106

OUTPUT_ENABLED = 0x80000000

class df(Enum):
    MAIN = 1        | OUTPUT_ENABLED
    ERROR = 2       | OUTPUT_ENABLED
    INVENTORY = 3   | OUTPUT_ENABLED
    ITEM = 4        | OUTPUT_ENABLED
    NODE = 5        | OUTPUT_ENABLED
    TRADER = 6      | OUTPUT_ENABLED

debugColors = {
    df.MAIN: bcolors.BLUE,
    df.ERROR: bcolors.LIGHTRED,
    df.ITEM: bcolors.LIGHTGREEN,
    df.INVENTORY: bcolors.LIGHTYELLOW,
    df.NODE: bcolors.LIGHTMAGENTA,
    df.TRADER: bcolors.LIGHTCYAN
}
debugBackgroundColors = {
    df.MAIN: None,
    df.ERROR: None,
    df.ITEM: None,
    df.INVENTORY: None,
    df.NODE: None,
    df.TRADER: None
}

# We add 11 spaces for the debug string, plus 2 for the brackets
DEBUG_NEWLINE = "\n" + " " * 13

def debug(flag:df, content:str):
    if not (flag.value & OUTPUT_ENABLED):
        return
    dbg_str = f"{flag}"
    if debugBackgroundColors[flag] == None:
        print(f"\033[1;{debugColors[flag]}m[{dbg_str[3:]:<11}]\033[0;39m" + content)
    else:
        print(  f"\033[1;{debugColors[flag]};{debugBackgroundColors[flag]}"
                f"m[{dbg_str[3:]:<11}]\033[0;39;49m" + content  )
