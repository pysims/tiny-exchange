import random
from typing import Any, Union

import yaml

import util
from util import debug, df

# Connections are right, up, left, down
# Connection list always needs to contain 4 entries
X_SIGN = [1, 0, -1, 0]
Y_SIGN = [0, -1, 0, 1]
X_LINE = "-"
Y_LINE = "|"
EAST = 0
NORTH = 1
WEST = 2
SOUTH = 3

class Node:
    """Here the business happens. Things are bought and sold.
    """
    #* STATIC VARIABLES
    
    node_list: "list[Node]" = []
    node_names: "list[str]"

    @staticmethod
    def loadInfo():
        with open(util.NODE_NAME_PATH, 'r') as stream:
            try:
                content = yaml.safe_load(stream)
                Node.node_names = content['names']
                debug(df.NODE, "Loaded node infos.")
            except yaml.YAMLError as exc:
                debug(df.ERROR, str(exc))
                Node.node_names = ["MISSING"]

    #* INSTANCE VARIABLES
    name: str
    traders: "list[Any]"
    connections: "list[Union[Node, None]]"

    def __init__(self):
        self.name = random.choice(Node.node_names)
        Node.node_names.remove(self.name)
        Node.node_list.append(self)
        self.traders = []
        self.connections = [None for _ in range(4)]
    
    def __del__(self):
        Node.node_list.remove(self)

    def drawGraph(self, depth:int=1, buffer:int=4):
        string_size = 1 + depth * 8
        string_size_vert = 1 + depth * 10
        graph_as_list = [[" " * buffer for _ in range(string_size)]
                            for _ in range(string_size_vert)]
        visited:"list[Node]" = []
        self.recursiveDrawGraph(graph_as_list,
                string_size//2, string_size_vert//2, visited, depth, buffer)
        final_text = "\n".join(["".join(i) for i in graph_as_list])
        debug(df.NODE, f"Drawing graph near {self.name}:")
        print(final_text)
        
    def recursiveDrawGraph(self, graph_as_list:"list[list[str]]",
            x:int, y:int, visited:"list[Node]", depth:int, buffer:int):
        # we add our own node and our own name to the graph list
        if self in visited:
            print("Skipping self.")

        visited.append(self)
        # We draw the node
        # graph_as_list[y][x] = "O" + " " * (buffer - 1)
        # We add our name (could replace the O with the name)
        graph_as_list[y][x] = f"{self.name[:buffer]:<{buffer}}"
        # We add the amount of traders
        graph_as_list[y + 1][x] = f"{len(self.traders):<{buffer}}"

        if depth > 0:
            counter = 0
            for node in self.connections:
                if node and not node in visited:
                    # Draw lines between nodes
                    vert:bool = (counter % 2 != 0)
                    if vert:
                        left = buffer // 2
                        right = buffer // 2 - (buffer % 2 == 0)
                        for i in range(1, buffer + 1):
                            if counter == 1 and i == buffer:
                                continue
                            if counter == 3 and i == 1:
                                continue
                            graph_as_list[y + Y_SIGN[counter] * i][x] =  \
                                left * " " + Y_LINE + right * " "
                    else:
                        for i in range(1, buffer):
                            graph_as_list[y][x + X_SIGN[counter] * i] =  \
                                X_LINE * buffer

                    node.recursiveDrawGraph(graph_as_list,
                        x + X_SIGN[counter] * buffer,
                        y + Y_SIGN[counter] * (buffer + 1),
                        visited, depth - 1, buffer)
                counter += 1
