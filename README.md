# tiny-exchange

A small-scale simulation of a free market, similar to a stock-exchange.

I am using the Pylance extension for VSCode, with strict type-hinting enabled.

# TODO overview

- Traders at nodes
    - Traders are at one specific node and can only trade with other traders at that node.
    - Movement between nodes
    - paths between nodes (either hierarchy like Dangerous Elite or as a weighted graph)
- Skills
    - For example, increased selling/decreased buying, more carry weight, etc

_tick: this function will be called on time passage, should be present for every class (at least as a pass)
