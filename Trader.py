import random

import yaml

import util
from Inventory import Inventory
from Item import Item
from Node import Node
import math
from util import debug, df

CONSUMES_PRICE_MULTIPLIER = 1.5
PRODUCES_PRICE_MULTIPLIER = 0.5

SELL_MULTIPLIER = 0.9
BUY_MULTIPLIER = 1.1

INDEX_NEW = 0
INDEX_SUM = 1

class Trader:
    """The traders have an inventory of items, some can move from Node to Node.
    """
    #* STATIC VARIABLES
    trader_list: "list[Trader]" = []
    traders_by_nodes: "dict[Node, list[Trader]]" = {}
    trader_names: "list[str]"

    @staticmethod
    def loadInfo():
        with open(util.TRADER_NAME_PATH, 'r') as stream:
            try:
                content = yaml.safe_load(stream)
                Trader.trader_names = content['names']
                debug(df.TRADER, "Loaded trader names.")
            except yaml.YAMLError as exc:
                debug(df.ERROR, str(exc))
                Trader.trader_names = ["MISSING"]

    @staticmethod
    def getConsumerList(node:Node) -> "list[Trader]":
        if not node in Trader.traders_by_nodes or not Trader.traders_by_nodes[node]:
            return []
        return [trader for trader in Trader.traders_by_nodes[node]
                if trader.consumes]

    @staticmethod
    def getProducerList(node:Node) -> "list[Trader]":
        if not node in Trader.traders_by_nodes or not Trader.traders_by_nodes[node]:
            return []
        return [trader for trader in Trader.traders_by_nodes[node]
                if trader.produces]

    #* INSTANCE VARIABLES
    node: "Node"
    can_travel: bool
    produces: "dict[str, list[int]]"
    consumes: "dict[str, list[int]]"

    def __init__(self,  starting_node: Node,
                        produces:"dict[str, list[int]]",
                        consumes:"dict[str, list[int]]",
                        name:str=""):
        if not Trader.trader_names:
            debug(df.ERROR, "Out of Trader names!")
        if not name:
            self.name = random.choice(Trader.trader_names)
            # For now, this is an easy way to make sure we have unique names
            Trader.trader_names.remove(self.name)
        else:
            self.name = name
        self.inventory = Inventory()
        self.node = starting_node
        self.node.traders.append(self)
        self.addToNodeList()
        self.can_travel = True
        self.produces = produces
        self.consumes = consumes
        debug(df.NODE, f"Node {self.name} was created.")
        for key, info in self.produces.items():
            debug(df.NODE, f"We produce {info[INDEX_NEW]}x {key}")
        for key, info in self.consumes.items():
            debug(df.NODE, f"We consume {info[INDEX_NEW]}x {key}")
        Trader.trader_list.append(self)

    def __del__(self):
        Trader.trader_list.remove(self)

    def moveToNode(self, node_target: Node):
        if self.node == node_target:
            return
        if self.node:
            if self in self.node.traders:
                self.node.traders.remove(self)
            else:
                debug(df.ERROR, "Missing in trader list!")
            self.removeFromNodeList()
        self.node = node_target
        self.node.traders.append(self)
        self.addToNodeList()

    def addToNodeList(self):
        # Trader can only be at one Node at any given time, let's check that's the case
        for inode, traderlist in  Trader.traders_by_nodes.items():
            if self in traderlist:
                debug(df.ERROR, f"Trader already in list for node {inode.name}")
                assert(False)
        if not self.node in Trader.traders_by_nodes:
            Trader.traders_by_nodes[self.node] = [self]
        else:
            Trader.traders_by_nodes[self.node].append(self)

    def removeFromNodeList(self):
        if not self in Trader.traders_by_nodes[self.node]:
            debug(df.ERROR, "Trader not in list, but trying to remove!")
            assert(False)
        Trader.traders_by_nodes[self.node].remove(self)

    def canTrade(self, other_trader: "Trader"):
        # For now, anyone on the same node can trade
        return (self.node == other_trader.node)

    def getName(self) -> str:
        return self.name

    def getInventory(self) -> "Inventory":
        return self.inventory

    def getTradersAtSameNode(self) -> "list[Trader]":
        return [trader for trader in Trader.trader_list if trader.node == self.node]

    def buyItem(self, other_trader: "Trader", item_id: str, amount: int = 1):
        """Buy the items from the other trader.

        Args:
            other_trader (Trader): The person we're buying from.
            item_id (str): The item to buy.
            amount (int, optional): The amount to buy. Defaults to 1.
        """
        self.inventory.buyItem(other_trader.inventory, item_id, amount)

    def sellItem(self, other_trader: "Trader", item_id: str, amount: int=1):
        other_trader.buyItem(self, item_id, amount)

    def getConsumerListAtSameNode(self) -> "list[Trader]":
        return [trader for trader in Trader.traders_by_nodes[self.node]
                if trader != self and trader.consumes]

    def getProducerListAtSameNode(self) -> "list[Trader]":
        return [trader for trader in Trader.traders_by_nodes[self.node]
                if trader != self and trader.produces]

    # Normal traders just won't need consume/produce lists
    def getConsumeItemIds(self) -> "list[str]":
        return [item_id for item_id in self.consumes]

    def getProduceItemIds(self) -> "list[str]":
        return [item_id for item_id in self.produces]

    def addConsumeItem(self, item_id: str, amount: int):
        if item_id in self.consumes:
            self.consumes[item_id][INDEX_NEW] += amount
        else:
            self.consumes[item_id][INDEX_NEW]  = amount

    def removeConsumeItem(self, item_id: str, amount: int):
        if item_id in self.consumes:
            self.consumes[item_id][INDEX_NEW] = \
                max(0, self.consumes[item_id][INDEX_NEW] - amount)

    def addProduceItem(self, item_id: str, amount: int):
        if item_id in self.produces:
            self.produces[item_id][INDEX_NEW] += amount
        else:
            self.produces[item_id][INDEX_NEW]  = amount

    def removeProduceItem(self, item_id: str, amount: int):
        if item_id in self.produces:
            self.produces[item_id][INDEX_NEW] = \
                max(0, self.produces[item_id][INDEX_NEW] - amount)

    def tick(self):
        if not self.consumes and not self.produces:
            return
        debug(df.NODE, f"Tick for [{self.name}]")
        for item_id, info_list in self.consumes.items():
            old_amount = self.inventory.getItemAmount(item_id)
            self.inventory.removeItem(item_id, info_list[INDEX_NEW], False)
            amount_delta = old_amount - self.inventory.getItemAmount(item_id)
            self.consumes[item_id][INDEX_SUM] += \
                amount_delta
            debug(df.NODE, f"Consumed {amount_delta} of {item_id}")
            if item := self.inventory.getItemById(item_id):
                real_basevalue = int(Item.getItemEntry(item_id)["basevalue"])
                item.basevalue = math.ceil(real_basevalue * CONSUMES_PRICE_MULTIPLIER)

        for item_id, info_list in self.produces.items():
            old_amount = self.inventory.getItemAmount(item_id)
            self.inventory.addItem(item_id, info_list[INDEX_NEW])
            amount_delta = self.inventory.getItemAmount(item_id) - old_amount
            self.produces[item_id][INDEX_SUM] += amount_delta
            debug(df.NODE, f"Produced {amount_delta} of {item_id}")
            if item := self.inventory.getItemById(item_id):
                real_basevalue = int(Item.getItemEntry(item_id)["basevalue"])
                item.basevalue = max(
                    math.floor(real_basevalue * PRODUCES_PRICE_MULTIPLIER), 1)
        
