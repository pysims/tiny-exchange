# Let's create a basic economy structure:
# We need nodes, where things are bought/sold
# we need buyers/sellers, who can be at any one node at a certain time
# the buyers/sellers all have unique items/requests
# -> the price at a node is dependend on the traders' properties
# we need items, with a set price, weight etc


# Objects: Node -> Trader -> Inventory -> item
# Hierarchy for importing
import random

from Item import Item
from Node import Node
from Trader import Trader
from util import debug, df

debug(df.MAIN, "Loading item info...")
Item.loadInfo()
debug(df.MAIN, "Loading trader info...")
Trader.loadInfo()
debug(df.MAIN, "Loading node info...")
Node.loadInfo()


debug(df.MAIN, "Hello there!")

traders: "list[Trader]" = []
nodes_per_name: "dict[str, Node]" = {}
for i in range(len(Node.node_names)):
    node = Node()
    consumes:"dict[str, list[int]]" = {}
    for j in range(random.randint(1,2)):
        consumes[random.choice(Item.getAllItemIds())] = \
            [random.randint(2,5), 0]
    produces:"dict[str, list[int]]" = {}
    for j in range(random.randint(1,2)):
        available = Item.getAllItemIds()
        for key in consumes:
            available.remove(key)
        rchoice = random.choice(available)
        produces[rchoice] = [random.randint(2,5), 0]
    new_trader = Trader(node, produces, consumes, node.name)
    new_trader.can_travel = False
    new_inv = new_trader.getInventory()
    # new_inv.addItem("water", random.randint(0,100))
    # new_inv.addItem("iron-bar", random.randint(0,50))
    # new_inv.addItem("bread", random.randint(0,200))
    new_inv.addCurrency(10000)
    traders.append(new_trader)
    nodes_per_name[node.name] = node

#TODO: connections read in from file?
# We need to connect the nodes
nodes_per_name["Eisenstadt"].connections = [
    None,
    None,
    nodes_per_name["Wien"],
    None
]
nodes_per_name["Wien"].connections = [
    nodes_per_name["Eisenstadt"],
    None,
    nodes_per_name["Linz"],
    None
]
nodes_per_name["Linz"].connections = [
    nodes_per_name["Wien"],
    None,
    nodes_per_name["Salzburg"],
    nodes_per_name["Graz"]
]
nodes_per_name["Salzburg"].connections = [
    nodes_per_name["Linz"],
    None,
    None,
    nodes_per_name["Innsbruck"]
]
nodes_per_name["Innsbruck"].connections = [
    nodes_per_name["Graz"],
    nodes_per_name["Salzburg"],
    None,
    None
]
nodes_per_name["Graz"].connections = [
    None,
    nodes_per_name["Linz"],
    nodes_per_name["Innsbruck"],
    None
]

#* We create a few traders and give them some inventory
for i in range(random.randint(4,6)):
    new_trader = Trader(random.choice(Node.node_list), {}, {})
    new_inv = new_trader.getInventory()
    # new_inv.addItem("water", random.randint(0,100))
    # new_inv.addItem("iron-bar", random.randint(0,50))
    # new_inv.addItem("bread", random.randint(0,200))
    new_inv.addCurrency(100)
    traders.append(new_trader)

print("\n\n")

total_money = 0
total_items = {}
for item in Item.getAllItemIds():
    total_items[item] = 0
total_items_amount = 0
total_items_amount_from_items = 0
total_weight = 0.0
total_weight_from_items = 0.0

for trader in traders:
    debug(df.MAIN, "-" * 15)
    debug(df.MAIN, f"{trader.name} is at {trader.node.name}. Inventory:")
    trader.getInventory().print()
    total_money += trader.getInventory().currency
    total_items_amount += trader.getInventory().getCurrentQuantity()
    total_weight += trader.getInventory().getCurrentWeight()
    for item in trader.getInventory().getAllItems():
        total_items[item.id] += item.amount
        total_weight_from_items += item.amount * item.weight
        total_items_amount_from_items += item.amount
    trader.tick()
    print("\n")

debug(df.MAIN, "-" * 30 + "\n\n")
debug(df.MAIN, "Arrived at mainloop!")

#* Now, let's trade some things!

ticks = random.randint(10, 20)

for i in range(ticks):
    debug(df.MAIN, "New selling loop!")
    # Printing trader locations
    for inode, trader_list in Trader.traders_by_nodes.items():
        traders_string = ", ".join([i.name for i in trader_list])
        debug(df.MAIN, f"Traders at {inode.name}: {traders_string}")

    for trader in traders:
        if not trader.can_travel:
            continue
        # Let's trade with the node!
        trader_items = [item.id for item in trader.inventory.items]
        node_consumer_list = trader.getConsumerListAtSameNode()
        if node_consumer_list:
            other_trader = node_consumer_list[0]
            trade_consume_items = [item for item in other_trader.consumes]
            choices = Item.intersectByID(trader_items, trade_consume_items)
            if choices:
                choice = random.choice(list(choices))
                debug(df.MAIN, f"{trader.name} is selling to {other_trader.name}:")
                # The trader sells all of his items
                trader.sellItem(other_trader, choice,
                                trader.inventory.getItemAmount(choice))

        # We buy from the node, but not every time
        node_producer_list = trader.getProducerListAtSameNode()
        if node_producer_list:
            other_trader = random.choice(node_producer_list)
            choices = other_trader.getProduceItemIds()
            while choices:
                choice = random.choice(list(choices))
                item = other_trader.inventory.getItemById(choice)
                if not item or item.maxAmountByCurrency(1,
                                trader.inventory.currency) == 0:
                    choices.remove(choice)
                    continue
                debug(df.MAIN, f"{trader.name} is buying from {other_trader.name}:")
                trader.buyItem(other_trader, choice, random.randint(100,1000))
                break
        
        # Let the people trade!
        trader_list = [tr for tr in trader.getTradersAtSameNode() if tr != trader and tr.can_travel]
        if len(trader_list) < 1:
            continue
        while True:
            other_trader = random.choice(trader_list)
            if trader.canTrade(other_trader):
                break
        if trader.getInventory().getCurrentQuantity() == 0:
            debug(df.MAIN, f"Trader {trader.name} has no items left.")
            debug(df.MAIN, "-" * 10)
            continue
        debug(df.MAIN, f"{trader.name} is selling to {other_trader.name}:")
        # trader.buyItem(other_trader, random.choice(
        #     other_trader.inventory.items).id, random.randint(1, 100))
        trader.sellItem(other_trader, random.choice(trader.inventory.items).id,
                random.randint(1,100))
        debug(df.MAIN, "-" * 5)
        
    for tr in traders:
        debug(df.MAIN, "-" * 15)
        debug(df.MAIN, f"{tr.name}'s inventory:")
        tr.getInventory().print()

    debug(df.MAIN, "Moving, producing and consuming!")
    for trader in traders:
        trader.tick()
        if not trader.can_travel:
            continue
        if trader.inventory.getCurrentQuantity() == 0:
            trader.moveToNode(random.choice(Node.node_list))
        else:
            tries = 10
            node = None
            trader_items = [item.id for item in trader.inventory.items]
            while tries > 0:
                node = random.choice(Node.node_list)
                node_consumer_list = Trader.getConsumerList(node)
                if node_consumer_list:
                    other_trader = random.choice(node_consumer_list)
                    node_consume_items = [item for item in other_trader.consumes]
                    if Item.intersectByID(trader_items, node_consume_items):
                        break
                tries -= 1
            if node:
                trader.moveToNode(node)


    # Let's print the map around the graph that's most visited.
    temp_node:Node = Node.node_list[0]
    for node in Node.node_list:
        if len(node.traders) > len(temp_node.traders):
            temp_node = node
    
    temp_node.drawGraph(depth=2)

    print("\n")

for trader in traders:
    debug(df.MAIN, "-" * 25)
    debug(df.MAIN, (f"{trader.name}'s inventory:"))
    trader.getInventory().print()
    print("\n")

debug(df.MAIN, "-" * 40)
for trader in Trader.trader_list:
    debug(df.MAIN, f"{trader.name} @ {trader.node.name}")


debug(df.MAIN, "-" * 20)
for name in Node.node_list:
    debug(df.MAIN, name.name)

final_total_money = 0
final_total_items = {}
for item in Item.getAllItemIds():
    final_total_items[item] = 0
final_total_items_amount = 0
final_total_weight = 0.0
final_total_weight_from_items = 0.0
final_total_items_amount_from_items = 0

for trader in traders:
    final_total_money += trader.getInventory().currency
    final_total_items_amount += trader.getInventory().getCurrentQuantity()
    final_total_weight += trader.getInventory().getCurrentWeight()
    for item in trader.getInventory().getAllItems():
        final_total_items[item.id] += item.amount
        final_total_weight_from_items += item.amount * item.weight
        final_total_items_amount_from_items += item.amount

    
# Now we need to add the produced and substract the consumed items.
for trader in Trader.trader_list:
    INDEX_NEW = 0
    INDEX_SUM = 1
    
    for item_id, info in trader.consumes.items():
        total_items_amount -= info[INDEX_SUM]
        total_items[item_id] -= info[INDEX_SUM]
        total_items_amount_from_items -= info[INDEX_SUM]
        temp_weight:float = Item.getItemEntry(item_id)["weight"] # type: ignore
        total_weight_from_items -= info[INDEX_SUM] * temp_weight
        total_weight -= info[INDEX_SUM] * temp_weight
    for item_id, info in trader.produces.items():
        total_items_amount += info[INDEX_SUM]
        total_items[item_id] += info[INDEX_SUM]
        total_items_amount_from_items += info[INDEX_SUM]
        temp_weight:float = Item.getItemEntry(item_id)["weight"] # type: ignore
        total_weight_from_items += info[INDEX_SUM] * temp_weight
        total_weight += info[INDEX_SUM] * temp_weight

if final_total_money != total_money:
    debug(df.ERROR, f"Missmatch in money: is {final_total_money}"
                    f" should be {total_money}")
    exit(-1)
if final_total_items_amount != total_items_amount:
    debug(df.ERROR, f"Missmatch in items amount: is {final_total_items_amount}"
                    f" should be {total_items_amount}")
    exit(-2)
if final_total_items_amount_from_items != total_items_amount_from_items:
    debug(df.ERROR, f"Missmatch in items amount from items: "
                    f"is {final_total_items_amount_from_items}"
                    f" should be {total_items_amount_from_items}")
    exit(-3)
if final_total_weight != total_weight:
    debug(df.ERROR, f"Missmatch in weight: is {final_total_weight}"
                    f" should be {total_weight}")
    exit(-4)
if final_total_weight_from_items != total_weight_from_items:
    debug(df.ERROR, f"Missmatch in item weight:"
                    f" is {final_total_weight_from_items}"
                    f" should be {total_weight_from_items}")
    exit(-5)
for key, value in final_total_items.items():
    if value != total_items[key]:
        debug(df.ERROR, f"Missmatch for {key} amount: "
                        f"{value} should be {total_items[key]}")
        exit(-6)

