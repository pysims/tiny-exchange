
import math
from typing import Union
import yaml

import util
from util import debug, df

class Item:
    """The actual item, contains all the attributes.
    """
    #* STATIC VARIABLES
    # Not every item needs to provide every value.
    default_dict: "dict[str, Union[str, int, float]]" = {
        "id": "error",
        "name": "missing-name",
        "weight": 1,
        "basevalue": 0,
        "amount": 0
    }
    items_dicts: "list[dict[str, Union[str, int, float]]]"

    @staticmethod
    def loadInfo():
        with open(util.ITEM_INFOS_PATH, 'r') as stream:
            try:
                content = yaml.safe_load(stream)
                Item.items_dicts = content['items']
                debug(df.ITEM, "Loaded item infos.")
            except yaml.YAMLError as exc:
                debug(df.ERROR, str(exc))
                Item.items_dicts = []

    @staticmethod
    def getItemEntry(item_id: str) -> "dict[str, Union[int, float, str]]":
        overwrite_dict = [i for i in Item.items_dicts if i["id"] == item_id]
        if overwrite_dict:
            return overwrite_dict[0]
        return Item.default_dict

    @staticmethod
    def getAllItemIds() -> "list[str]":
        return [str(item["id"]) for item in Item.items_dicts if "id" in item]

    @staticmethod
    def getName(item_id: str):
        return Item.getItemEntry(item_id)["name"]

    @staticmethod
    def intersectByID(listA: "list[str]", listB: "list[str]") -> "list[str]":
        return [item for item in listA if item in listB]

    #* INSTANCE VARIABLES
    id: str
    name: str
    weight: float
    basevalue: int
    amount: int

    def __init__(self, item_id: str):
        properties = {}
        properties.update(Item.default_dict)
        overwrite_dict = Item.getItemEntry(item_id)
        properties.update(overwrite_dict)
        for key, value in properties.items():
            if key == "id":
                self.id = value
            elif key == "name":
                self.name = value
            elif key == "weight":
                self.weight = value
            elif key == "basevalue":
                self.basevalue = value
            elif key == "amount":
                self.amount = value

    def addAmount(self, amount: int):
        self.amount += amount

    def removeAmount(self, amount: int):
        self.amount = max(0, self.amount - amount)

    def maxAmountByCurrency(self, desired_amount:int,
                    currency_amount:int, price_multiplier:float=1.0) -> int:
        return min(desired_amount,
            math.floor(currency_amount / (self.basevalue * price_multiplier)),
                    self.amount)

    def maxAmountByWeight(self, desired_amount:int, remaining_weight:float) -> int:
        return min(desired_amount, math.floor(remaining_weight / self.weight))

    def print(self, number_length:int, name_length:int):
        debug(df.ITEM, f"{self.name:<{name_length}}: {self.amount:>{number_length}}x "
              f"@ {self.basevalue:>{number_length}}"
              f"{util.CURRENCY_SHORT}"
              f" | {round(self.weight, 1):>{number_length+2}.1f}"
              f"{util.WEIGHT_SHORT} each "
              f"/ {round(self.weight * self.amount, 1):>{number_length+2}.1f}"
              f"{util.WEIGHT_SHORT} total")
