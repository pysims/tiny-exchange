import math
from random import randint
from typing import Union

import util
from Item import Item
from util import debug, df


class Inventory:
    """The actual container for items, with useful functions bunched up.
    """

    #* INSTANCE VARIABLES
    items: "list[Item]"
    currency: int
    max_weight: float
    max_quantity: int

    def __init__(self):
        self.items = []
        self.currency = 0
        self.max_weight = randint(500, 2500)
        self.max_quantity = randint(500, 2500)

    def getAllItems(self) -> "list[Item]":
        return self.items

    def getCurrentWeight(self) -> float:
        weight = 0
        for item in self.items:
            weight += item.weight * item.amount
        if weight > self.max_weight:
            debug(df.INVENTORY, "[WARN] Weight has exceeded max weight!")
        return weight

    def getRemainingWeight(self) -> float:
        return self.max_weight - self.getCurrentWeight()

    def getRemainingQuantitiy(self) -> int:
        return self.max_quantity - self.getCurrentQuantity()

    def getCurrentQuantity(self) -> int:
        quantity = 0
        for item in self.items:
            quantity += item.amount
        if quantity > self.max_quantity:
            debug(df.INVENTORY, "[WARN] Item Amount has exceeded max amount!")
        if quantity < 0:
            debug(df.INVENTORY, "[ERROR] Item Amount below 0!")
        return quantity

    def getItemById(self, item_id: str) -> Union["Item",None]:
        found = [i for i in self.items if i.id == item_id]
        if found:
            return found[0]
        return None

    def getItemAmount(self, item_id: str) -> int:
        item = self.getItemById(item_id)
        if item != None and item.amount != None:
            return item.amount # type: ignore
        else:
            return 0

    def addCurrency(self, amount: int) -> int:
        if amount > 0:
            self.currency += amount
        else:
            debug(df.ERROR, "Currency below 0!")
        return self.currency

    def removeCurrency(self, amount: int) -> int:
        if amount > 0:
            self.currency -= amount
        if self.currency < 0:
            debug(df.ERROR, "Negative Currency for Inventory!")
        return self.currency

    def addItem(self, item_id: str, amount: int = 1):
        if amount <= 0:
            return
        # Let's check remaining amount
        amount = min(amount, self.getRemainingQuantitiy())
        debug(df.INVENTORY, f"Adding {amount} x {item_id}")
        found = self.getItemById(item_id)
        if found:
            # Let's check for remaining weight
            amount = found.maxAmountByWeight(amount, self.getRemainingWeight())
            if not amount:
                debug(df.INVENTORY, "No Weight remaining...")
                return
            debug(df.INVENTORY, f"Item exists, adding {amount}"
                                f" to amount {found.amount}!")
            found.addAmount(amount)
        else:
            new_item = Item(item_id)
            # Let's check for remaining weight
            amount = new_item.maxAmountByWeight(amount, self.getRemainingWeight())
            if not amount:
                debug(df.INVENTORY, "No Weight remaining...")
                return
            new_item.addAmount(amount)
            self.items.append(new_item)

    def removeItem(self, item_id: str, amount: int = 1, warn:bool=True):
        if amount <= 0:
            return
        found = self.getItemById(item_id)
        if found:
            found.removeAmount(amount)
            if not found.amount:
                debug(df.INVENTORY, f"Removed last of {item_id}.")
                self.items.remove(found)
            else:
                debug(df.INVENTORY, f"Removed {amount} -> {found.amount}")
        elif warn:
            debug(df.INVENTORY, "[WARN] Trying to remove item we do not have!")
            return

    def buyItem(self, other_inv: "Inventory", item_id: str,
                    amount: int = 1, buying: bool = True):
        verb = "buy" if buying else "sell"
        debug(df.INVENTORY, f"Trying to {verb} {amount} {Item.getName(item_id)}")
        wanted_item = other_inv.getItemById(item_id)
        if not wanted_item:
            debug(df.INVENTORY, f"Could't find item "
                                f"{item_id} in the other inventory.")
            return

        price_multiplier = 1.0
        # if buying:
        #     price_multiplier = 1.1
        # else:
        #     price_multiplier = 0.9

        # Let's see how many items we can really buy!
        max_items = wanted_item.maxAmountByCurrency(amount,
                                        self.currency, price_multiplier)
        debug(df.INVENTORY, f"Max items by currency: {max_items} @ "
                    f"{round(wanted_item.basevalue * price_multiplier)} each")
        max_items = wanted_item.maxAmountByWeight(max_items,
                                        self.getRemainingWeight())
        debug(df.INVENTORY, f"Max items by weight: {max_items}")
        max_items = min(max_items, self.getRemainingQuantitiy())
        debug(df.INVENTORY, f"Max items by quantity: {max_items}")

        if buying:
            total_price = math.ceil(max_items * \
                    (wanted_item.basevalue * price_multiplier))
        else:
            total_price = math.floor(max_items * \
                    (wanted_item.basevalue * price_multiplier))
        debug(df.INVENTORY, f"Maximum amount of items possible: "
                        f"{max_items} / {amount}" +
                        f" for a total of {total_price}{util.CURRENCY_SHORT}")
        
        if not max_items:
            return
        # Let's see if we need to add the item
        self.addItem(item_id, max_items)
        self.removeCurrency(total_price)
        other_inv.removeItem(item_id, max_items)
        other_inv.addCurrency(total_price)


    def print(self):
        # TODO: calc numbers dynamically
        number_length = 4
        name_length = 8
        current_quantity = self.getCurrentQuantity()
        total_item_value = 0
        for item in self.items:
            total_item_value += item.basevalue * item.amount
        debug(df.INVENTORY, (
                f"Money: {self.currency:>{number_length+2}}"
                f"{util.CURRENCY_SHORT}"
                f" (Total Value: {self.currency+total_item_value:>{number_length+2}}"
                f"{util.CURRENCY_SHORT}) | "
                f"{current_quantity:>{number_length}} "
                f"/ {self.max_quantity:>{number_length}} items | "
                f"{self.getCurrentWeight():>{number_length+util.FLOAT_PAD_EXTRA}.1f} "
                f"/ {self.max_weight:>{number_length+util.FLOAT_PAD_EXTRA}.1f}"
                f" {util.WEIGHT_SHORT}"))
        for item in self.items:
            item.print(number_length, name_length)
