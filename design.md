# Quick Prototype

## Goal
Create a working/playable prototype in 2-3 weeks.

## Basic idea
A simulated free market, with cities providing products, and other cities consuming products.  
Prices are dynamic and can be influenced by the player.

## Gameplay elements
- Overview over current products (prices differ by city)
- Overivew over production/consumption rates
- Buying/Selling items
- Making decisions that influence the economy (for example, open a new smithy in a town -> weapon price decreases)

## Interaction
Probably via console, maybe using very simple gui (pyQt, tkinter, ...)

## Backend
Data structures should be expandable, do things as modular as possible, so new features can be added in the future.
