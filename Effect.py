from enum import Enum

class EffectType(Enum):
    CHANGE_CARRY_WEIGHT = 1,
    CHANGE_CARRY_AMOUNT = 2,
    CHANGE_SELL_PRICE = 3,
    CHANGE_BUY_PRICE = 4


"""every effect instances has duration, amount, etc"""
# effect A with amount 5% and dur 5min
# effect A with amount +2 and dur 2min
# Is it even possible to combine?! -> Let's just say no for now
class Effect:
    """This can generate boosts, improve stats temporarily, be used for skills, etc.
    """
    duration: float # ticks remaining
    amount: float # amount to change stat by
    absolute: bool
    effect: EffectType
    def __init__(self, effect: EffectType, amount: float, absolute: bool, duration: int):
        self.amount = amount
        self.absolute = absolute
        self.effect = effect
        self.duration = duration

    def tick(self):
        self.duration -= 1.0

    def isOver(self) -> bool:
        return (self.duration <= 0 or self.amount == 0.0)


    


